package edu.umn.csci1902.project4;

/**
 * 	This class is to be used for processing the shortest path. It will
 * 	record the current location being processed and the path that it is
 * 	on.
 * 
 * 
 * @author Andrew Erickson eric1796
 *
 */
public class WorkEntry implements Comparable<WorkEntry> {
	public final String label;
	public final Path path;

	
	public WorkEntry(String l, Path p){
		this.label = l;
		this.path = p;
	}

	@Override
	public int compareTo(WorkEntry o) {
		return Double.compare(this.path.weight, o.path.weight);
	}

}
