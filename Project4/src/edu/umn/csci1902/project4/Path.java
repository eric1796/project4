package edu.umn.csci1902.project4;

import java.util.ArrayList;
import java.util.List;
/**
 * 	This class creates an object that records a weight and list
 * 	of strings that keeps track of how it arrived to its
 * 	current location. Only used when looking for neighbor paths
 * 	using a breadth-first search.
 * 
 * @author Andrew Erickson eric1796
 *
 */
public class Path {
	public double weight;
	public List<String> path;
	
	/**
	 * 	default construct creates an empty list of strings and sets
	 * 	zero weight
	 */
	public Path(){
		this.path = new ArrayList<String>();
		this.weight = 0.0;
	}
	
	/**
	 * 	constructor that sets the list and weight to the given parameters
	 * @param list
	 * @param weight
	 */
	public Path(List<String> list, double weight){
		this.path = list;
		this.weight = weight;
	}

}
