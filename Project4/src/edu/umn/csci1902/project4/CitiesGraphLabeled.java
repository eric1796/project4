package edu.umn.csci1902.project4;

import java.util.List;
import java.util.Map;
/**
 * 	This class extends the CitiesGraph for functionality specific
 * 	to a labeled type that accounts for start city, end city and label.
 * 
 * @author Andrew Erickson eric1796
 *
 */
public class CitiesGraphLabeled extends CitiesGraph {

	/* (non-Javadoc)
	 * @see edu.umn.csci1902.project4.CitiesGraph#printShortestPath(java.lang.String, java.lang.String)
	 */
	@Override
	public void printShortestPath(String startCity, String endCity) {
		Map<String, Path> paths = this.getNeighborPaths(startCity);
		System.out.println("leave " + startCity);
		List<String> pathList = paths.get(endCity).path;
		int pathLength = pathList.size();
		for (int i = 0; i < pathLength - 1; i++) {
			System.out.print("take "
					+ this.getGraph().get(pathList.get(i))
							.get(pathList.get(i + 1)).label);
			System.out.println(" to " + pathList.get(i + 1));
		}
		System.out.println("Distance: " + (int) paths.get(endCity).weight);

	}

}
