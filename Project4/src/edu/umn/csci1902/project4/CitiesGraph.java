package edu.umn.csci1902.project4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

/**
 * 	This class serves as a wrapper class for the graph to organize the needed
 * 	methods for finding the and outputing the shortest path.
 * 
 * @author Andrew Erickson eric1796
 *
 */
public class CitiesGraph{

	private Map<String, Map<String, EdgeInfo>> graph;

	/**
	 * creates an empty graph
	 */
	public CitiesGraph() {
		this.graph = new HashMap<>();
	}


	/**
	 * Builds a map by associated cities by there edges
	 * 
	 * @param startCity	the start city of the edge
	 * @param endCity	the destination city of the edge
	 * @param label	the label of the edge
	 * @param edgeWeight	the weight of the edge for weighted graphs
	 * @return	true if the edge was added
	 */
	public boolean addEdge(String startCity, String endCity, String label,
			double edgeWeight) {
		Map<String, EdgeInfo> neighbors = this.getGraph().get(startCity);
		if (neighbors == null) {
			neighbors = new HashMap<String, EdgeInfo>();
			this.getGraph().put(startCity, neighbors);
		}
		neighbors.put(endCity, new EdgeInfo(label, edgeWeight));

		return this.getGraph().get(startCity).containsKey(endCity);
	}


	
	/**
	 * 	Records the paths to each location from a starting location as it iterates
	 * 	through the graph from closest to farthest.
	 * 
	 * @param startCity
	 * @return a map of key String value Path
	 */
	public Map<String, Path> getNeighborPaths(String startCity) {
		Map<String, Path> paths = new HashMap<>();
		Set<String> finished = new HashSet<String>();
		Queue<WorkEntry> workQueue = new PriorityQueue<>();
		
		List<String> pathList = new ArrayList<>();
		pathList.add(startCity);
		paths.put(startCity, new Path(pathList,0.0));
		workQueue.add(new WorkEntry(startCity, new Path(pathList,0.0)));

		while (!workQueue.isEmpty()) {
			WorkEntry e = workQueue.remove();
			if (!finished.contains(e.label)) {
				finished.add(e.label);
				for (Map.Entry<String, EdgeInfo> nbr : this.getGraph()
						.get(e.label).entrySet()) {
					String nl = nbr.getKey();
					double nw = e.path.weight + nbr.getValue().weight;
					List<String> p = new ArrayList<>();
					p.addAll(e.path.path);
					p.add(nl);
					Path np = new Path(p,nw);
					if (!finished.contains(nl)
							&& (!paths.containsKey(nl) || paths.get(nl).weight > nw)) {
						paths.put(nl, np);
						workQueue.add(new WorkEntry(nl, np));
					}
				}
			}

		}

		return paths;
	}



	/**
	 * @return	returns the graph associated with this object
	 */
	public Map<String, Map<String, EdgeInfo>> getGraph() {
		return this.graph;
	}



	/**
	 * 	This method will print the shortest path using the information
	 * 	gathered from the
	 * 	{@link edu.umn.csci1902.project4.CitiesGraph#getNeighborPaths(java.lang.String)}
	 * 	method for the graph. If the graph is of a different sub-type, it is likely to 
	 * 	be overridden in that type to print its unique information.
	 * 
 * @see 
	 * @param startCity
	 * @param endCity
	 */
	public void printShortestPath(String startCity, String endCity) {

		Map<String, Path> paths = this.getNeighborPaths(startCity);
		System.out.println("leave "+ startCity);
		List<String> pathList = paths.get(endCity).path;
		int pathLength = pathList.size();
		for(int i =0;i<pathLength - 1;i++){
			System.out.print("take " + this.getGraph().get(pathList.get(i)).get(pathList.get(i+1)).label);
			System.out.println(" to " + pathList.get(i+1) + " (" + 
			(int) this.getGraph().get(pathList.get(i)).get(pathList.get(i+1)).weight + " km)");
		}
		System.out.println("Distance: "+ (int) paths.get(endCity).weight + " km");

}



}
