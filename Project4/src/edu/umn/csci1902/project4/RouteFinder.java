package edu.umn.csci1902.project4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
/**
 * 	The class takes exactly three arguments: a filename to a
 * 	file containing the type of graph to be built on the
 * 	first line. Then pairs of neighboring cities with
 * 	option of labels and weights between them.
 * 
 * 	The main method constructs a generic CitiesGraph object,
 * 	but the graph can be overridden by extended objects for
 * 	fine tune printing. Just uncomment the constructor within
 * 	the switch cases that matches the current mode.
 * 
 * 	The class checks whether the file exists and whether the
 * 	start and end cities are present in the graph.
 * 
 * 
 * @author Andrew Erickson eric1796
 *
 */
public class RouteFinder {
	

	/**
	 *	-checks the that the arguments are exactly three
	 *	-builds a new RouteFinder
	 *	-builds a new CitiesGraph
	 * 	-checks if the first argument describes a mode that can be read
	 * 	-switches between the three predetermined modes to build the graph accordingly
	 * 		-NOTE: uncomment the different types of graph for printing unique to the mode
	 * 	-checks whether the second argument is in the graph
	 * 	-checks if the third argument is in the the graph
	 * 	-prints the shortest path of the graph
	 * 
	 * 
	 * @param args	Exactly 3 arguments: file name, starting city,
	 * 				and ending city
	 */
	public static void main(String[] args) {
		if(args.length == 3){
	    CitiesGraph graph = new CitiesGraph();
		try (Scanner scan = new Scanner(new File(args[0]))) {
			String mode = scan.nextLine();
			if(!(mode.equals("basic")
					|| mode.equals("labeled")
					|| mode.equals("weighted"))){
				System.err.println("File cannot be read");
				System.exit(1);
			}
			switch(mode){
			case "basic":
				//graph = new CitiesGraphBasic();
				while (scan.hasNext()) {
					String startCity = scan.next();
					String endCity = scan.next();
					graph.addEdge(startCity, endCity, "road", 1.0);
					graph.addEdge(endCity, startCity, "road", 1.0);
				}
				break;
			case "labeled":
				//graph = new CitiesGraphLabeled();
				while (scan.hasNext()) {
					String label = scan.next();
					String startCity = scan.next();
					String endCity = scan.next();
					graph.addEdge(startCity, endCity, label, 1.0);
					graph.addEdge(endCity, startCity, label, 1.0);
				}
				break;
			case "weighted":
				//graph = new CitiesGraphWeighted();
				while (scan.hasNext()) {
					String label = scan.next();
					String startCity = scan.next();
					String endCity = scan.next();
					double weight = Double.parseDouble(scan.next());
					graph.addEdge(startCity, endCity, label, weight);
					graph.addEdge(endCity, startCity, label, weight);
				}
				break;
			}
			
		} catch (FileNotFoundException e) {
			System.err.println("The following file not found: "+args[0]);
			e.printStackTrace();
			System.exit(1);
		}
		
		if(!graph.getGraph().containsKey(args[1])){
			System.err.println("Could not locate element: "+args[1]);
			System.exit(1);
		}
		
		if(!graph.getGraph().containsKey(args[2])){
			System.err.println("Could not locate element: "+args[2]);
			System.exit(1);
		}
		
		
		graph.printShortestPath(args[1], args[2]);
		

	}else{
		System.err.println("Progam requires exactly 3 arguments");
		System.exit(1);
	}

}
}
