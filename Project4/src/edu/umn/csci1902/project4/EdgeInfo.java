package edu.umn.csci1902.project4;
/**
 * 	This class records the edge label and the weight associated
 * 	with the edge. Used when building a graph.
 * 
 * 
 * @author Andrew Erickson eric1796
 *
 */
public class EdgeInfo {
	public final String label;
	public final double weight;
	
	/**
	 * Default constructor has no label and 1 as weight
	 */
	public EdgeInfo(){
		this.label = "";
		this.weight = 1.0;
	}
	
	/**
	 * 	contructor to set label and weight of the edge
	 * @param label
	 * @param weight
	 */
	public EdgeInfo(String label, double weight){
		this.label = label;
		this.weight = weight;
	}
}
