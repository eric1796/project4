<!-- Original Author: Michael Ekstrand
     Adapted: Emery Mizero

     A "project" describes a set of targets that may be requested
     when Ant is executed.  The "default" attribute defines the
     target which is executed if no specific target is requested,
     and the "basedir" attribute defines the current working directory
     from which Ant executes the requested task.  This is normally
     set to the current working directory.

     Ant projects often define properties, which are just names with
     values that will be substituted when the name is used elsewhere
     in the ant project.
-->
<!-- Version: 2.1 -->
<project name="Project4" default="run">
  <property name="level.attempted"
            value="C"/>
  
  <property name="srcdir" value="src"/>
  <!-- If you use a 'test' directory, change this property. -->
  <property name="testdir" value="src"/>

  <property name="bindir" value="bin"/>
  <property name="libdir" value="${basedir}/lib"/>
  <property name="docdir" value="docdir"/>

  <property name="app.class" value="edu.umn.csci1902.project4.RouteFinder"/>
  <property name="app.fork" value="no"/>

  <property name="zipname" value="Project4"/>
  <property name="zipname.issued" value="${zipname}-issued"/>
  <property name="doczip" value="${zipname}-doc"/>
  <property name="build.sysclasspath" value="ignore"/>
  <property name="junit.enabled" value="true"/>
  <property name="lib.junit.jar" value="${libdir}/junit.jar"/>
  <property name="lib.junit.sha1.expected"
            value="e4f1766ce7404a08f45d859fb9c226fc9e41a861"/>

  <property name="input.file" value="basic1.dat"/>
  <property name="node.start" value="Chicago"/>
  <property name="node.end" value="GrandForks"/>
        
  <path id="lib.classpath">
    <fileset dir="${libdir}">
      <include name="*.jar"/>
    </fileset>
  </path>

  <!-- ==================== Setup Target ================================ -->

  <!--
  The "setup" target sets up the local environment, pulling in things like JUnit.
  -->
  
  <target name="setup" depends="lib.junit"/>

  <target name="init">
    <mkdir dir="${libdir}"/>
    <condition property="lib.junit.present">
      <available file="${libdir}/junit.jar"/>
    </condition>
    <condition property="testdir.separate">
      <not>
        <equals arg1="${testdir}" arg2="${srcdir}"/>
      </not>
    </condition>
  </target>

  <target name="fetch.junit" depends="init" unless="lib.junit.present">
    <get src="http://search.maven.org/remotecontent?filepath=junit/junit/4.10/junit-4.10.jar"
         dest="${libdir}/junit.jar"/>
  </target>

  <target name="lib.junit" depends="fetch.junit" if="junit.enabled">
    <checksum file="${lib.junit.jar}"
              property="lib.junit.sha1"
              algorithm="SHA-1"/>
    <condition property="lib.junit.good">
      <equals arg1="${lib.junit.sha1}" arg2="${lib.junit.sha1.expected}"/>
    </condition>
    <fail message="JUnit JAR corrupted. Run 'ant full-clean' and try again."
          unless="lib.junit.good"/>
  </target>

  <target name="lib.all" depends="lib.junit"/>

  <!-- ==================== Depend Target ================================ -->

  <!--
  The "depends" target removes any class files that were compiled
  based on other Java files that have since changed.  These files
  are out-of-date, and need to be rebuilt.
  -->
  <target name="depend">
    <depend srcDir="${srcdir}"
            destDir="${bindir}"
            closure="yes"/>
    <depend srcDir="${testdir}"
            destDir="${bindir}"
            closure="yes"/>
  </target>

  <!-- ==================== Build Target  =============================== -->
  <!--
  The "build" target compiles all of your Java files into .class
  files that are ready to run.
  -->
  <target name="build" depends="depend,lib.all">
    <mkdir dir="${bindir}"/>
    <echo message="building app from ${srcdir}"/>
    <javac source="1.7"
           target="1.7"
           debug="yes"
           srcdir="${srcdir}"
           destdir="${bindir}"
           includes="**/*.java">
    	<classpath refid="lib.classpath"/>
    </javac>
  </target>

  <target name="build.test" depends="init,build,depend,lib.all"
          if="testdir.separate">
    <echo message="building tests from ${testdir}"/>
    <mkdir dir="${bindir}"/>
    <javac source="1.7"
           target="1.7"
           debug="yes"
           srcdir="${testdir}"
           destdir="${bindir}"
           includes="**/*.java">
      <classpath refid="lib.classpath"/>
      <classpath path="${bindir}"/>
    </javac>
  </target>

  <!-- ==================== Test Target  =============================== -->
  <!-- The "test" target runs your JUnit tests.  -->
  <target name="test" depends="build.test,lib.all">
    <junit printsummary="yes" fork="yes"
           haltonfailure="no" failureproperty="tests.failed">
      <classpath refid="lib.classpath"/>
      <classpath path="${bindir}"/>
      <formatter type="plain"/>
      <batchtest>
        <fileset dir="${srcdir}">
          <include name="**/*Test.java"/>
        </fileset>
        <fileset dir="${testdir}">
          <include name="**/*Test.java"/>
        </fileset>
      </batchtest>
    </junit>
    <!--
        we use this and failureproperty rather than haltonfailure so
        that it runs and reports on all tests, even after failing, but
        still reports the overall build as a failure.  haltonfailure
        aborts the build after the first failed test.
      -->
    <fail if="tests.failed"/>
  </target>

  <!-- RUN target -->
  <!-- The "run" target runs the application -->
  <target name="run" depends="build">
    <java classname="${app.class}" fork="${app.fork}">
      <classpath path="${bindir}"/>
      <classpath refid="lib.classpath"/>
      <arg value="${input.file}"/>
      <arg value="${node.start}"/>
      <arg value="${node.end}"/>
    </java>
  </target>

  <!-- ==================== Clean Target  =============================== -->

  <!--
  The "clean" target deletes all the compiled files.  Clean is used both to
  save space, and to make sure a compile starts out fresh.
  -->
  <target name="clean">
    <delete dir="${bindir}"/>
    <delete dir="${docdir}"/>
    <delete dir=".">
      <include name="TEST-*.txt"/>
      <include name="${doczip}"/>
    </delete>
  </target>

  <target name="full-clean" depends="clean">
    <delete dir="${libdir}"/>
  </target>

  <!-- ==================== Doc Target  ========================== -->
  <!--
      The "doc" target builds JavaDoc for your code.
    -->
  <target name="doc">
    <javadoc
       destdir="${docdir}"
       source="1.7"
       package="yes">
      <sourcefiles>
        <fileset dir="${srcdir}">
          <include name="**/*.java"/>
          <exclude name="**/*Test.java"/>
        </fileset>
      </sourcefiles>
      <link href="http://java.sun.com/javase/7/docs/api/"/>
    </javadoc>
    <zip destfile="${doczip}"
         basedir="${basedir}"
         includes="doc/**/*"/>
  </target>

  <!-- ==================== Package Target ============================== -->

  <!--
  The "package" target creates a package of all your Java files, suitable for
  turning in to a grader - or distributing to a customer.
  -->
  <target name="package">
    <delete file="${zipname}.tgz"/>
    <zip destfile="${basedir}/${zipname}.zip">
      <zipfileset dir="${basedir}" prefix="${zipname}">
        <include name="${srcdir}/**/*.java"/>
        <include name="${testdir}/**/*.java"/>
        <include name="**/*.txt"/>
        <exclude name="**/TEST-*.txt"/>
        <include name="build.xml"/>
      </zipfileset>
    </zip>
    <script language="javascript">
      var out = java.lang.System.out
      var zipfile = zipname + ".zip"
      out.println("Contents of " + zipfile + ":");
      var zfile = new java.util.zip.ZipFile(new java.io.File(zipfile));
      var entries = zfile.entries();
      while (entries.hasMoreElements()) {
        var entry = entries.nextElement();
        out.println(entry.getName());
      }
    </script>
  </target>

  <!-- issue-package - creates a package to publish to students -->
  <target name="issue-package">
    <tar destfile="${zipname.issued}.tgz" compression="gzip">
      <tarfileset dir="${basedir}" prefix="${zipname}">
        <include name="src/**/*.java"/>
        <include name="build.xml"/>
      </tarfileset>
    </tar>
    <zip destfile="${zipname.issued}.zip" compress="yes">
      <zipfileset dir="${basedir}" prefix="${zipname}">
        <include name="src/**/*.java"/>
        <include name="build.xml"/>
      </zipfileset>
    </zip>
  </target>

</project>

