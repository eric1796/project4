/**
 * 
 */
package edu.umn.csci1902.project4;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test class for CitiesGraph class
 * @author Andrew Erickson eric1796
 *
 */
public class CitiesGraphTest {
	private static CitiesGraph graph;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		graph = new CitiesGraph();
		graph.addEdge("Bismark", "Fargo", "I94", 300);
		graph.addEdge("Fargo", "Bismark", "I94", 300);
		graph.addEdge("Minneapolis", "Chicago", "I94", 583);
		graph.addEdge("Chicago", "Minneapolis", "I94", 583);
		graph.addEdge("St. Paul", "Chicago", "I94", 554);
		graph.addEdge("Chicago", "St. Paul", "I94", 554);
		graph.addEdge("Minneapolis", "St. Paul", "I94", 31);
		graph.addEdge("St. Paul", "Minneapolis", "I94", 31);
		graph.addEdge("Minneapolis", "Fargo", "I94", 333);
		graph.addEdge("Fargo", "Minneapolis", "I94", 333);
		graph.addEdge("Fargo", "GrandForks", "I29", 128);
		graph.addEdge("GrandForks", "Fargo", "I29", 128);
		
	}



	/**
	 * Test method for {@link edu.umn.csci1902.project4.CitiesGraph#getNeighborPaths(java.lang.String)}.
	 * Tests the accumulated weights to a particular destination.
	 * Tests that the method chooses the smallest weight
	 */
	@Test
	public void testGetNeighborPathsWeights() {
		Map<String, Path> paths = graph.getNeighborPaths("Fargo");
		assertTrue(paths.get("Fargo").weight == 0.0);
		assertTrue(paths.get("Bismark").weight == 0.0 + 300.0);
		assertTrue(paths.get("St. Paul").weight == 0.0 + 333.0 + 31.0);
		paths = graph.getNeighborPaths("Bismark");
		assertTrue(paths.get("Chicago").weight == 0.0 + 300.0 + 333.0 + 583.0);


	}
	
	
	/**
	 * Test method for {@link edu.umn.csci1902.project4.CitiesGraph#getNeighborPaths(java.lang.String)}.
	 * Tests that the accumulated paths to a particular destination.
	 * Tests that the method chooses the shortest accumulated path
	 */
	@Test
	public void testGetNeighborPathsPaths(){
		Map<String, Path> paths = graph.getNeighborPaths("Fargo");
		List<String> path = new ArrayList<>();
		path.add("Fargo");
		path.add("Minneapolis");
		path.add("St. Paul");
		assertEquals(paths.get("St. Paul").path , path);
		paths = graph.getNeighborPaths("Bismark");
		path = new ArrayList<>();
		path.add("Bismark");
		path.add("Fargo");
		path.add("Minneapolis");
		path.add("Chicago");
		assertEquals(paths.get("Chicago").path , path);
	}


}
