Andrew Erickson
eric1796

git: https://eric1796@bitbucket.org/eric1796/project4.git

I am attempting A level.

I consulted no one other than course staff for this assignment (other than
the web for answers and description section of this README.txt; link is
below).

I only consulted textbook, course materials, and Java API documentation for
this assignment.

Answers and descriptions:

C-level: 

State the data structures you used in creating your solution, how
you used them, and why you chose the ones you did.

At first, I was thinking of building my own graph ADT based on an interface
written in the 2nd edition of the textbook. However, since we were using Maps
in class to build the graphs, much of the methods given in the interface
proved to be unnecessary for the scope of this assignment. For example, I found
that I would never be using isEmpty() or hasEdge().

I decided to stick with using Maps creating a Path class to record the path from
a start and end point. I wrapped the graph in a CitiesGraph class so that it would
more readable and to add the main methods for getting information from the graph. 
Although, it did require that I call a getGraph() method every time I wanted to use
the graphs elements.

B-level:

Describe what data structures you used to represent the data needed to do
routefinding and output on labeled graphs.

I decided to create an EdgeInfo class that would store the label of particular edge.
Instead of having a List<String> as Map values to represent neighbors, I used a
Map<String, EdgeInfo>. So the graph was of type Map<String, Map<String,EdgeInfo>>.

A-level:

Suppose a graph has multiple roads between two cities. Is this useful? Will the
alternate roads ever be taken? What changes to the problem might make them
useful?

It would be useful if the distances were shorter. In java's HashMap, if the map
previously contained a mapping for the key being added, the old value is replaced.
So only the last values added would be compared.

You could make the EdgeInfo store a PriorityQueue<Double> for the weights instead of storing
a single weight when building the graph. Then whenever the weight is compared when looking
for the shortest path, it will always compare the shortest weight for the edge.
Consulted the following link:
http://stackoverflow.com/questions/4838400/java-hashmap-duplicate-elements

